package br.com.tdd.leilao;

import br.com.tdd.leilao.Lance;
import br.com.tdd.leilao.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LanceTeste {

    Usuario user;
    Lance lance;

    @BeforeEach
    public void setUp(){
        this.user = new Usuario(1,"Laura");
        this.lance = new Lance(user, 1000.00);
    }

    @Test
    public void testaImpressaoValorLanceEmReais(){
        String valorAdquirido = this.lance.valorLanceEmReais();
        String valorEsperado = "R$1.000,00";
        Assertions.assertEquals(valorAdquirido, valorEsperado);
    }

}
