package br.com.tdd.leilao;

import br.com.tdd.leilao.Lance;
import br.com.tdd.leilao.Leilao;
import br.com.tdd.leilao.Leiloeiro;
import br.com.tdd.leilao.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

//O Leiloeiro deve ter um método que retorna o maior lance e de quem é aquele lance.
public class LeiloeiroTeste {

    private Lance lance1;
    private Lance lance2;
    private Usuario user1;
    private Usuario user2;
    private Leilao leilao;
    private Leiloeiro leiloeiro;

    @BeforeEach
    public void setUp(){
        this.user1 = new Usuario(1,"Mateus");
        this.user2 = new Usuario(2,"Laura");

        this.lance1 = new Lance(user1, 500.00);
        this.lance2 = new Lance(user2, 1000.00);

        List<Lance> lancesSetup = new ArrayList<>();
        lancesSetup.add(lance1);
        lancesSetup.add(lance2);
        this.leilao = new Leilao(lancesSetup);

        this.leiloeiro = new Leiloeiro("Bisteca", this.leilao);
    }

    @Test
    public void testaMaiorLanceDoMeuLeilao(){
        Leiloeiro leiloeiroTeste = this.leiloeiro;
        String mensagemExecutada = leiloeiroTeste.apresentaMaiorLance();
        String nome = lance2.getUsuario().getNome();
        String mensagemEsperada = "Dono do lance: " + nome + " / Valor: " + lance2.valorLanceEmReais();
        Assertions.assertEquals(mensagemEsperada, mensagemExecutada);
    }
}
