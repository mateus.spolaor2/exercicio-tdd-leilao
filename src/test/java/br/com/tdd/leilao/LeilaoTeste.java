package br.com.tdd.leilao;

import br.com.tdd.leilao.Lance;
import br.com.tdd.leilao.Leilao;
import br.com.tdd.leilao.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTeste {

    private Leilao leilao;
    private Lance lance;
    private Usuario user;

    @BeforeEach
    public void setUp(){
        this.user = new Usuario(1,"Mateus");

        this.lance = new Lance(user, 500.00);

        List<Lance> lances = new ArrayList<>();
        this.leilao = new Leilao(lances);
    }

    //O leilão deve ter um método que permite adicionar novo lance.
    @Test
    public void adicionaUmNovoLance(){

        Lance lanceAdicionado;
        lanceAdicionado = this.lance;

        this.leilao.adicionarNovoLance(lanceAdicionado);
        Assertions.assertEquals(this.lance, lanceAdicionado);
    }


    //O leilão não pode aceitar lance menor que o anterior.
    @Test
    public void testaSeOLanceEhMaiorQueOAnterior (){

        this.leilao.adicionarNovoLance(this.lance);

        Lance novoLance = new Lance(user, 1000.00);
        boolean isValid = this.leilao.validaAdicaoLance(novoLance);
        Assertions.assertTrue(isValid);
    }

    @Test
    public void testaSeEhOPrimeiroLance (){
        boolean isValid = this.leilao.validaAdicaoLance(this.lance);
        Assertions.assertTrue(isValid);
        Assertions.assertEquals(this.leilao.getLances().size(), 0);
    }

}
