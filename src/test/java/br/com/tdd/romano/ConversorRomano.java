package br.com.tdd.romano;

public class ConversorRomano {

    public String converterCasaDaUnidade(int numeroParaConverter) {
        if(numeroParaConverter == 4){
            return "IV";
        } else if (numeroParaConverter == 9) {
            return "IX";
        }

        String algarismoConvertido = "";
        int valorMedio = 5;
        int casaAuxiliar = numeroParaConverter-valorMedio;

        if(numeroParaConverter < 4){
            algarismoConvertido = adicionaMenorUnidade(numeroParaConverter, "I");
            return algarismoConvertido;
        } else {
            algarismoConvertido = "V";
            algarismoConvertido += adicionaMenorUnidade(casaAuxiliar, "I");
            return algarismoConvertido;
        }
    }

    public String converterCasaDasDezenas(int numeroParaConverter) {
        int primeiroAlgarismo = numeroParaConverter/10;
        int ultimoAlgarismo  = numeroParaConverter%10;
        if(numeroParaConverter >= 40 && numeroParaConverter < 50){
            return "XL" + converterCasaDaUnidade(ultimoAlgarismo);
        } else if (numeroParaConverter >= 90 && numeroParaConverter < 90){
            return "XC" + converterCasaDaUnidade(ultimoAlgarismo);
        }

        String algarismoConvertido = "X";
        int valorMedio = 5;
        int casaAuxiliar = primeiroAlgarismo-valorMedio;

        if(primeiroAlgarismo < 4){
            algarismoConvertido = adicionaMenorUnidade(primeiroAlgarismo, "X");
            return algarismoConvertido += converterCasaDaUnidade(ultimoAlgarismo);
        } else {
            algarismoConvertido = "L";
            algarismoConvertido += adicionaMenorUnidade(casaAuxiliar, "X");
            return algarismoConvertido += converterCasaDaUnidade(ultimoAlgarismo);
        }
    }

    public String converterCasaDasCentenas(int numeroParaConverter) {
        int primeiroAlgarismo = numeroParaConverter/100;
        int ultimoAlgarismo  = numeroParaConverter%10;
        int segundoAlgarismo = (numeroParaConverter%100);

        if(numeroParaConverter >= 400 && numeroParaConverter < 500){
            return "CD" + converterCasaDasDezenas(segundoAlgarismo);
        } else if (numeroParaConverter >= 900 && numeroParaConverter < 900){
            return "CM" + converterCasaDasDezenas(segundoAlgarismo);
        }

        String algarismoConvertido = "C";
        int valorMedio = 5;
        int casaAuxiliar = primeiroAlgarismo-valorMedio;

        if(primeiroAlgarismo < 4){
            algarismoConvertido = adicionaMenorUnidade(primeiroAlgarismo, "C");
            return algarismoConvertido + converterCasaDasDezenas(segundoAlgarismo);
        } else {
            algarismoConvertido = "D";
            algarismoConvertido += adicionaMenorUnidade(casaAuxiliar, "C");
            return algarismoConvertido + converterCasaDasDezenas(segundoAlgarismo);
        }
    }
    private String adicionaMenorUnidade(int numeroParaConverter, String letraReferente) {
        String retorno = "";
        for (int i = 0; i < numeroParaConverter; i++) {
            retorno += letraReferente;
        }
        return retorno;
    }

}
