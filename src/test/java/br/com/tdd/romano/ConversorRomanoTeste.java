package br.com.tdd.romano;

/*
Um número maior à frente de um número menor significa subtração.
A adição é caracterizada por um número menor à frente de um número maior. Por exemplo, IV significa 4, VI significa 6.

Não se utiliza mais de um número menor antes de um número maior para subtrair.
Por exemplo, IIV não corresponde a 3.

Devem separar-se, dezenas, centenas, e milhares como numerais separados.
Isso significa que a 99 corresponde XCIX (90 + 9), nunca deve ser escrito como IC. Similarmente, 999 não pode ser IM e 1999 não pode ser MIM.

I	Corresponde ao numeral 1. II são dois, III são três, IV são 4 (ocasionalmente pode ver-se IIII como 4)
V	Corresponde ao numeral 5. IV são 4, VI são 6, VII são 7, VIII são 8.
X	Corresponde ao numeral 10. IX são 9, XI é 11, etc..
L	Corresponde ao numeral 50. XL é o 40.
C	Corresponde ao numeral 100. C tem origem na palavra latina Centum.
D	Corresponde ao numeral 500.
M 	Corresponde ao numeral 1000.
*/


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ConversorRomanoTeste {

    ConversorRomano conversorRomano;

    @BeforeEach
    public void setup(){
        this.conversorRomano = new ConversorRomano();
    }

    @Test
    public void testaConversaoComUmaCasa(){

        int numeroParaConverter = 9;
        String resultadoConversao = conversorRomano.converterCasaDaUnidade(numeroParaConverter);

        Assertions.assertEquals(resultadoConversao, "IX");

    }

    @Test
    public void testaConversaoCasaDasDezenas(){

        int numeroParaConverter = 41;
        String resultadoConversao = conversorRomano.converterCasaDasDezenas(numeroParaConverter);

        Assertions.assertEquals(resultadoConversao, "LXVII");
    }

    @Test
    public void testaConversaoCasaDasCentenas(){

        int numeroParaConverter = 433;
        String resultadoConversao = conversorRomano.converterCasaDasCentenas(numeroParaConverter);

        Assertions.assertEquals(resultadoConversao, "CDXXXIII");
    }

}
