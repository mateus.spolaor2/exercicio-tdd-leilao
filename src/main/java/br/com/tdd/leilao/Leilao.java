package br.com.tdd.leilao;

import java.util.List;

public class Leilao {

    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao(){}

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void adicionarNovoLance(Lance lance) {
        this.lances.add(lance);
    }

    public Boolean validaAdicaoLance(Lance lance){

        int ultimoIndice = this.lances.size() - 1;
        if(ultimoIndice < 0) return true;

        double valorUltimoLance = this.lances.get(ultimoIndice).getValorLance();
        if(lance.getValorLance() > valorUltimoLance){
            return true;
        } else {
            return false;
        }
    }

}
