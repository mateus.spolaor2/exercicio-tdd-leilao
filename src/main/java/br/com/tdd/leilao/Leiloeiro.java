package br.com.tdd.leilao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Leiloeiro {

    private String nome;

    private Leilao leilao;

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public Leiloeiro(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public String apresentaMaiorLance() {
        List<Lance> lances = this.leilao.getLances();
        Lance maiorLance = Collections.max(lances, Comparator.comparing(s -> s.getValorLance()));
        String nome = maiorLance.getUsuario().getNome();
        return "Dono do lance: " + nome + " / Valor: " + maiorLance.valorLanceEmReais();
    }
}
