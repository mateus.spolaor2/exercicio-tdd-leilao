package br.com.tdd.leilao;

import java.text.DecimalFormat;

public class Lance {

    private Usuario usuario;
    private double valorLance;

    public Lance(Usuario usuario, double valorLance) {
        this.usuario = usuario;
        this.valorLance = valorLance;
    }

    public Lance() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public double getValorLance() {
        return valorLance;
    }

    public void setValorLance(double valorLance) {
        this.valorLance = valorLance;
    }

    public String valorLanceEmReais() {
        DecimalFormat decimalFormat = new DecimalFormat("R$" + "#,##0.00");
        return decimalFormat.format(this.valorLance);
    }
}
